# CHANGELOG

## 1.0.1 ( )

* `.gitlab-ci.yml` based on `c2platform.core`.

## 1.0.0 ( 2023-06-16 )

* New pipeline script.

## 0.1.7 ( 2022-09-29 )

* Migrated this collection to [Gitlab](https://gitlab.com/c2platform/ansible-collection-forgerock).