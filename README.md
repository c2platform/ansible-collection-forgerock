# Ansible Collection - c2platform.forgerock

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-forgerock/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-forgerock/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/forgerock/)

Roles for [ForgeRock](https://www.forgerock.com/) platform.

## Roles

* [ds](./roles/ds) ForgeRock Directory Services.
* [am](./roles/am) ForgeRock Access Management.
* [ig](./roles/ig) ForgeRock Identity Gateway.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/forgerock/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.forgerock
ansible-doc -t filter --list c2platform.forgerock
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
>