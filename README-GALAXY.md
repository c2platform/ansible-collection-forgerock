# Ansible Collection - c2platform.forgerock

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-forgerock/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-forgerock/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines)

See full [README](https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/README.md).
